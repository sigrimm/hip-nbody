# README #

### Setup ###

Check GPUs
lshw -class display


Choose the platform with either

export HIP_PLATFORM=amd

or

export HIP_PLATFORM=nvidia


check the installaion with:

/opt/rocm/bin/hipconfig --full

should look similar to

== hipconfig
HIP_PATH     : /opt/rocm-4.1.0/hip
ROCM_PATH    : /opt/rocm-4.1.0
HIP_COMPILER : clang
HIP_PLATFORM : nvidia
HIP_RUNTIME  : rocclr



### HIP00 ###

This directory contains a very simple code which calls a GPU kernel and prints 'hello' from each thread.
It contains a CUDA (.cu) and a HIP (.cpp) version.

compile with:

nvcc -o test00 test00.cu

hipcc -o test00hip test00.cpp

test00Hipify.cpp is a translation of test00.cu

### integrator ###

This directory contains a simple symplectic N-body integrator. The code is designed to integrate
many sets of small planetary systems, for example TRAPPIST-1. It uses warp shufl instructions to
perform the Kick and SunKick operation. It uses a fg-function to solve the Kepler equation.
The initial conditions of 7 planets are given in the file initial7.dat.

`-Nst` sets the number of planetary systems. All of them are identical copies in this code.  
Each planetary system is assigned to a different thread block, this is not the most efficient way.

The code combides different operations into a single kernel, that saves kernel overhead time.

symplecticHipify.cpp is a translation of symplectic.cu, the shfl_sync functions are not translated correctly.


### Kick ###

This directory includes an N^2 kick kernel. The size of N can be set by `-N `

MainHipify is a translation of Main.cu, it has some problems:
deviceOverlap
template kernel call not correct
 
