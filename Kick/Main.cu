#include <cuda.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <sys/resource.h>

#include "GENGAKick.h"
#include "define.h"

__host__ void IC(T4 *x4_h, T4 *v4_h, int N){

	T earthmass = 3.0024584e-6;
	T m = 4.0 * earthmass / double(N);
	T r = 1.0e-5;

	srand (0);
	for(int i = 0; i < N; ++i){

		T a = drand48() * (4.0 - 0.5) + 0.5;   //in AU
		T e = drand48() * 0.1;
		T inc = drand48() * 0.1;
		T Omega = drand48() * 2.0 * M_PI; 
		T w = drand48() * 2.0 * M_PI; 
		T M = drand48() * 2.0 * M_PI; 


		double mu = 1.0;
		double E = M + e * 0.5;
		double Eold = E;

		for(int j = 0; j < 32; ++j){
			E = E - (E - e * sin(E) - M) / (1.0 - e * cos(E));
		if(fabs(E - Eold) < EPS) break;
			Eold = E;
		}

		double Px = cos(w) * cos(Omega) - sin(w) * cos(inc) * sin(Omega);
		double Py = cos(w) * sin(Omega) + sin(w) * cos(inc) * cos(Omega);
		double Pz = sin(w) * sin(inc);

		double Qx = -sin(w) * cos(Omega) - cos(w) * cos(inc) * sin(Omega);
		double Qy = -sin(w) * sin(Omega) + cos(w) * cos(inc) * cos(Omega);
		double Qz = cos(w) * sin(inc);

		double t1 = a * (cos(E) - e);
		double t2 = a * sqrt(1.0 - e * e) * sin(E);

		x4_h[i].x =  t1 * Px + t2 * Qx;
		x4_h[i].y =  t1 * Py + t2 * Qy;
		x4_h[i].z =  t1 * Pz + t2 * Qz;
		x4_h[i].w = m;

		double t0 = 1.0 / (1.0 - e * cos(E)) * sqrt(mu / a);
		t1 = -sin(E);
		t2 = sqrt(1.0 - e * e) * cos(E);

		v4_h[i].x = t0 * (t1 * Px + t2 * Qx);
		v4_h[i].y = t0 * (t1 * Py + t2 * Qy);
		v4_h[i].z = t0 * (t1 * Pz + t2 * Qz);
		v4_h[i].w = r;

//printf("%d %g %g %g %g %g %g\n", i, x4_h[i].x, x4_h[i].y, x4_h[i].z, v4_h[i].x, v4_h[i].y, v4_h[i].z);

	}
}


int main(int argc, char **argv){

	int N = 8192;

	int L = 1; //Number of iterations
	int dev = 0;

	int ndev = 1;	//number of GPUs

	//Read console input arguments
	for(int i = 1; i < argc; i += 2){
		if(strcmp(argv[i], "-N") == 0){
			N = atoi(argv[i + 1]);
		}
		else if(strcmp(argv[i], "-L") == 0){
			L = atoi(argv[i + 1]);
		}
		else if(strcmp(argv[i], "-dev") == 0){
			dev = atoi(argv[i + 1]);
		}
		else if(strcmp(argv[i], "-ndev") == 0){
			ndev = atoi(argv[i + 1]);
		}
	}

	printf("Using Device %d\n", dev);
	int devCount;
	cudaGetDeviceCount(&devCount);
	printf("There are %d CUDA devices.\n", devCount);

	cudaDeviceProp devProp;
	for(int i = 0; i < devCount; ++i){
		cudaGetDeviceProperties(&devProp, i);
		printf("Name:%s, Major:%d, Minor:%d, Max threads per Block:%d, Max x dim:%d, #Multiprocessors:%d, Can Map Memory:%d, Clock Rate:%d, Memory Clock Rate:%d, Can Overlap:%d, Concurrent Kernels %d\n",  devProp.name, devProp.major, devProp.minor, devProp.maxThreadsPerBlock, devProp.maxThreadsDim[0], devProp.multiProcessorCount, devProp.canMapHostMemory, devProp.clockRate, devProp.memoryClockRate, devProp.deviceOverlap, devProp.concurrentKernels);
	}

	int runtimeVersion;
	int driverVersion;
	cudaRuntimeGetVersion(&runtimeVersion);
	cudaDriverGetVersion(&driverVersion);
	printf("Runtime Version: %d\n", runtimeVersion);
	printf("Driver Version: %d\n", driverVersion);

	cudaSetDevice(0);
	cudaError_t error;

//	cudaDeviceSetCacheConfig(cudaFuncCachePreferShared);

	timeval tt1, tt2;
        long ms;


	T4 *x4_h, *x4_d;
	T4 *v4_h, *v4_d;

	x4_h = (T4*)malloc(N * sizeof(T4));
	v4_h = (T4*)malloc(N * sizeof(T4));


	cudaMalloc((void **)&x4_d, N * sizeof(T4));
	cudaMalloc((void **)&v4_d, N * sizeof(T4));

        error = cudaGetLastError();
        printf("error1 = %d = %s\n",error, cudaGetErrorString(error));

	//Initialize random values 
	IC(x4_h, v4_h, N);


	cudaMemcpy(x4_d, x4_h, N * sizeof(T4), cudaMemcpyHostToDevice);
	cudaMemcpy(v4_d, v4_h, N * sizeof(T4), cudaMemcpyHostToDevice);

        cudaDeviceSynchronize();
        error = cudaGetLastError();
        printf("error2 = %d = %s\n",error, cudaGetErrorString(error));

	FILE *TimeFile;
	char TimeFilename[160];

	sprintf(TimeFilename, "NTime.dat");

	TimeFile = fopen(TimeFilename, "a");

	cudaDeviceSynchronize();
	error = cudaGetLastError();
	printf("error3 = %d = %s\n",error, cudaGetErrorString(error));
	gettimeofday(&tt1, NULL);



	for(int t = 0; t < L; ++t){
		//Call Here the Kernel


		if(ndev > 1){
			//multi GPU
			int NN = (N + ndev - 1) / ndev;
			int N0 = 0;
			int N1 = NN;
			for(int i = 0; i < ndev; ++i){
			//for(int i = 0; i < 1; ++i){
				N1 = min(N1, N);
				cudaSetDevice(i);
				if(i > 0)cudaDeviceEnablePeerAccess(0, 0);		
				kick4B_kernel<128, 4> <<< dim3( ((NN + 3)/ 4) / 1, 1, 1), dim3(128,1,1) >>>(x4_d, v4_d, N0, N1, N);
				N0 += NN;
				N1 += NN;

			}
			cudaSetDevice(0);
		}
		else{
			kick4B_kernel<128, 4> <<< dim3( ((N + 3)/ 4) / 1, 1, 1), dim3(128,1,1) >>>(x4_d, v4_d, 0, N, N);

		}

	}

	cudaDeviceSynchronize();
        gettimeofday(&tt2, NULL);
        error = cudaGetLastError();
        printf("free error = %d = %s\n",error, cudaGetErrorString(error));

	cudaMemcpy(x4_h, x4_d, N * sizeof(T4), cudaMemcpyDeviceToHost);
	cudaMemcpy(v4_h, v4_d, N * sizeof(T4), cudaMemcpyDeviceToHost);

	for(int i = 0; i < N; ++i){
		printf("%g %g %g\n", v4_h[i].x, v4_h[i].y, v4_h[i].z);
	}


	ms = (1000000 * tt2.tv_sec + tt2.tv_usec - 1000000* tt1.tv_sec - tt1.tv_usec);
	printf("time %g\n", ms/1000000.0);

	fprintf(TimeFile, "%d %d %g %g\n", N, L, ms/1000000.0, ms/1000000.0 / ((double)(L)));
	fclose(TimeFile);



	free(x4_h);
	free(v4_h);
	cudaFree(x4_d);
	cudaFree(v4_d);
	printf("Done\n");

	return 0;
}
