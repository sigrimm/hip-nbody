#define USE_RSQRT 0             //1: use rsqrt() or rsqrtf(); 0: use 1.0 / sqrt() or 1.0f / sqrtf()

#define CONTROL 0               //can not be used for large N
#define DOUBLE 1                //0: single precision, 1: double precision

#define def_p 128		//tile sizes

#if(DOUBLE == 0)
        #define EPS 1.0e-15f    //Softening factor in single precision
        #define T4 float4
        #define T3 float3
        #define T float
        #define ZERO 0.0f
        #define ONE 1.0f
	#define FACT 0.1f
#else
        #define EPS 1.0e-30     //Softening factor in double precision
        #define T4 double4
        #define T3 double3
        #define T double
        #define ZERO 0.0
        #define ONE 1.0
	#define FACT 0.1
#endif

