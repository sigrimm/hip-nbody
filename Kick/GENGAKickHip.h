#include "define.h"
__device__ void  accG(T3 &ac, T4 &x4i, T4 &x4j){
	T rsq, ir, ir3, s;
	T3 r3ij;

	r3ij.x = x4j.x - x4i.x;
	r3ij.y = x4j.y - x4i.y;
	r3ij.z = x4j.z - x4i.z;

	rsq = r3ij.x*r3ij.x + r3ij.y*r3ij.y + r3ij.z*r3ij.z + EPS;

#if(USE_RSQRT == 0)
	#if(DOUBLE == 0)
		ir = ONE / sqrtf(rsq);
	#else
		ir = ONE / sqrt(rsq);
	#endif
#elif(USE_RSQRT == 1)
	#if(DOUBLE == 0)
		ir = rsqrtf(rsq);
	#else
		ir = rsqrt(rsq);
	#endif
#else
	ir = __frsqrt_rn(rsq);
#endif

	ir3 = ir*ir*ir;

	s = x4j.w * ir3;
		
	ac.x += r3ij.x * s;
	ac.y += r3ij.y * s;
	ac.z += r3ij.z * s;
}

template <const int Bl>
__global__ void kick16_kernel(T4 *x4_d, T4 *v4_d){
	int idy = threadIdx.x;
	int idx = blockIdx.x;

	__shared__ T3 ab1_s[2*Bl];

	T4 x4i = x4_d[idx];
	T4 x4j = x4_d[idy];

	ab1_s[idy].x = 0.0;
	ab1_s[idy].y = 0.0;
	ab1_s[idy].z = 0.0;
	ab1_s[idy + Bl].x = 0.0;
	ab1_s[idy + Bl].y = 0.0;
	ab1_s[idy + Bl].z = 0.0;

	__syncthreads();

	if(idy < Bl){
		accG(ab1_s[idy], x4i, x4j); 
	}

	__syncthreads();
	volatile T3 *ab1 = ab1_s;

	ab1[idy].x += ab1[idy + 8].x;
	ab1[idy].x += ab1[idy + 4].x;
	ab1[idy].x += ab1[idy + 2].x;
	ab1[idy].x += ab1[idy + 1].x;

	ab1[idy].y += ab1[idy + 8].y;
	ab1[idy].y += ab1[idy + 4].y;
	ab1[idy].y += ab1[idy + 2].y;
	ab1[idy].y += ab1[idy + 1].y;

	ab1[idy].z += ab1[idy + 8].z;
	ab1[idy].z += ab1[idy + 4].z;
	ab1[idy].z += ab1[idy + 2].z;
	ab1[idy].z += ab1[idy + 1].z;

	__syncthreads();

	if(idy == 0){
		v4_d[idx].x += ab1[0].x;
		v4_d[idx].y += ab1[0].y;
		v4_d[idx].z += ab1[0].z;
	}
}

template <const int Bl, int Bl2>
__global__ void kick32_kernel(T4 *x4_d, T4 *v4_d){
	int idy = threadIdx.x;
	int idx = blockIdx.x;

	__shared__ T3 a1_s[Bl2];

	T4 x4i = x4_d[idx];
	T4 x4j = x4_d[idy];

	a1_s[idy].x = 0.0;
	a1_s[idy].y = 0.0;
	a1_s[idy].z = 0.0;
	__syncthreads();
	if(idy < Bl){
		accG(a1_s[idy], x4i, x4j); 
	}

	__syncthreads();
	volatile T3 *a1 = a1_s;

	if(idy < 32){
		if(Bl >= 64) a1[idy].x += a1[idy + 32].x;
		if(Bl >= 32) a1[idy].x += a1[idy + 16].x;
		a1[idy].x += a1[idy + 8].x;
		a1[idy].x += a1[idy + 4].x;
		a1[idy].x += a1[idy + 2].x;
		a1[idy].x += a1[idy + 1].x;

		if(Bl >= 64) a1[idy].y += a1[idy + 32].y;
		if(Bl >= 32) a1[idy].y += a1[idy + 16].y;
		a1[idy].y += a1[idy + 8].y;
		a1[idy].y += a1[idy + 4].y;
		a1[idy].y += a1[idy + 2].y;
		a1[idy].y += a1[idy + 1].y;

		if(Bl >= 64) a1[idy].z += a1[idy + 32].z;
		if(Bl >= 32) a1[idy].z += a1[idy + 16].z;
		a1[idy].z += a1[idy + 8].z;
		a1[idy].z += a1[idy + 4].z;
		a1[idy].z += a1[idy + 2].z;
		a1[idy].z += a1[idy + 1].z;
	}

	__syncthreads();

	if(idy == 0){
		v4_d[idx].x += a1[0].x;
		v4_d[idx].y += a1[0].y;
		v4_d[idx].z += a1[0].z;
	}
}

template <int Bl>
__global__ void kick128_kernel(T4 *x4_d, T4 *v4_d, int N2){

	int idy = threadIdx.x;
	int idx = blockIdx.x;

	__shared__ T3 a1_s[Bl];
	__shared__ T3 a2_s[Bl];

	T4 x4i = x4_d[idx];
	T4 x4i2 = x4_d[idx + N2];
	T4 x4j = x4_d[idy];

	a1_s[idy].x = 0.0;
	a1_s[idy].y = 0.0;
	a1_s[idy].z = 0.0;
	
	a2_s[idy].x = 0.0;
	a2_s[idy].y = 0.0;
	a2_s[idy].z = 0.0;

	__syncthreads();
	accG(a1_s[idy], x4i, x4j);
	accG(a2_s[idy], x4i2, x4j);

	__syncthreads();
	volatile T3 *a1 = a1_s;
	volatile T3 *a2 = a2_s;

	if(idy < 64){
		a1[idy].x += a1[idy + 64].x;
		a1[idy].y += a1[idy + 64].y;
		a1[idy].z += a1[idy + 64].z;

		a2[idy].x += a2[idy + 64].x;
		a2[idy].y += a2[idy + 64].y;
		a2[idy].z += a2[idy + 64].z;

	}
	__syncthreads();

	if(idy < 32){
		a1[idy].x += a1[idy + 32].x;
		a1[idy].x += a1[idy + 16].x;
		a1[idy].x += a1[idy + 8].x;
		a1[idy].x += a1[idy + 4].x;
		a1[idy].x += a1[idy + 2].x;
		a1[idy].x += a1[idy + 1].x;

		a1[idy].y += a1[idy + 32].y;
		a1[idy].y += a1[idy + 16].y;
		a1[idy].y += a1[idy + 8].y;
		a1[idy].y += a1[idy + 4].y;
		a1[idy].y += a1[idy + 2].y;
		a1[idy].y += a1[idy + 1].y;

		a1[idy].z += a1[idy + 32].z;
		a1[idy].z += a1[idy + 16].z;
		a1[idy].z += a1[idy + 8].z;
		a1[idy].z += a1[idy + 4].z;
		a1[idy].z += a1[idy + 2].z;
		a1[idy].z += a1[idy + 1].z;
	}

	else{
		if(idy < 64){
			
			a2[idy-32].x += a2[idy + 32-32].x;
			a2[idy-32].x += a2[idy + 16-32].x;
			a2[idy-32].x += a2[idy + 8-32].x;
			a2[idy-32].x += a2[idy + 4-32].x;
			a2[idy-32].x += a2[idy + 2-32].x;
			a2[idy-32].x += a2[idy + 1-32].x;

			a2[idy-32].y += a2[idy + 32-32].y;
			a2[idy-32].y += a2[idy + 16-32].y;
			a2[idy-32].y += a2[idy + 8-32].y;
			a2[idy-32].y += a2[idy + 4-32].y;
			a2[idy-32].y += a2[idy + 2-32].y;
			a2[idy-32].y += a2[idy + 1-32].y;
	
			a2[idy-32].z += a2[idy + 32-32].z;
			a2[idy-32].z += a2[idy + 16-32].z;
			a2[idy-32].z += a2[idy + 8-32].z;
			a2[idy-32].z += a2[idy + 4-32].z;
			a2[idy-32].z += a2[idy + 2-32].z;
			a2[idy-32].z += a2[idy + 1-32].z;	

		}
	}

	__syncthreads();

	if(idy == 0){
			v4_d[idx].x += a1[0].x;
			v4_d[idx].y += a1[0].y;
			v4_d[idx].z += a1[0].z;
	}

	if(idy == 32){
			v4_d[idx + N2].x += a2[0].x;
			v4_d[idx + N2].y += a2[0].y;
			v4_d[idx + N2].z += a2[0].z;
	}
}

template <int Bl>
__global__ void kick256_kernel(T4 *x4_d, T4 *v4_d, int N, int N4){
	int idy = threadIdx.x;
	int idx = blockIdx.x;

	__shared__ T3 a1_s[Bl];
	__shared__ T3 a2_s[Bl];
	__shared__ T3 a3_s[Bl];
	__shared__ T3 a4_s[Bl];

	T4 x4i = x4_d[idx];
	T4 x4i2 = x4_d[idx + N4];
	T4 x4i3 = x4_d[idx + 2*N4];
	T4 x4i4 = x4_d[idx + 3*N4];

	a1_s[idy].x = 0.0;
	a1_s[idy].y = 0.0;
	a1_s[idy].z = 0.0;
	
	a2_s[idy].x = 0.0;
	a2_s[idy].y = 0.0;
	a2_s[idy].z = 0.0;

	a3_s[idy].x = 0.0;
	a3_s[idy].y = 0.0;
	a3_s[idy].z = 0.0;
	
	a4_s[idy].x = 0.0;
	a4_s[idy].y = 0.0;
	a4_s[idy].z = 0.0;

	__syncthreads();


	for(int i = 0; i < N; i += Bl){ 
		if(idy + i < N){
			T4 x4j = x4_d[idy + i];
			accG(a1_s[idy], x4i, x4j);
			accG(a2_s[idy], x4i2, x4j);
			accG(a3_s[idy], x4i3, x4j);
			accG(a4_s[idy], x4i4, x4j);
		}
	}
	__syncthreads();
	volatile T3 *a1 = a1_s;
	volatile T3 *a2 = a2_s;
	volatile T3 *a3 = a3_s;
	volatile T3 *a4 = a4_s;

	if(Bl >= 256){
		if(idy < 128){
			a1[idy].x += a1[idy + 128].x;
			a1[idy].y += a1[idy + 128].y;
			a1[idy].z += a1[idy + 128].z;

			a2[idy].x += a2[idy + 128].x;
			a2[idy].y += a2[idy + 128].y;
			a2[idy].z += a2[idy + 128].z;

			a3[idy].x += a3[idy + 128].x;
			a3[idy].y += a3[idy + 128].y;
			a3[idy].z += a3[idy + 128].z;

			a4[idy].x += a4[idy + 128].x;
			a4[idy].y += a4[idy + 128].y;
			a4[idy].z += a4[idy + 128].z;

		}
	}
	__syncthreads();

	if(idy < 64){
		a1[idy].x += a1[idy + 64].x;
		a1[idy].y += a1[idy + 64].y;
		a1[idy].z += a1[idy + 64].z;

		a2[idy].x += a2[idy + 64].x;
		a2[idy].y += a2[idy + 64].y;
		a2[idy].z += a2[idy + 64].z;

		a3[idy].x += a3[idy + 64].x;
		a3[idy].y += a3[idy + 64].y;
		a3[idy].z += a3[idy + 64].z;

		a4[idy].x += a4[idy + 64].x;
		a4[idy].y += a4[idy + 64].y;
		a4[idy].z += a4[idy + 64].z;

	}
	__syncthreads();

	if(idy < 32){
		a1[idy].x += a1[idy + 32].x;
		a1[idy].x += a1[idy + 16].x;
		a1[idy].x += a1[idy + 8].x;
		a1[idy].x += a1[idy + 4].x;
		a1[idy].x += a1[idy + 2].x;
		a1[idy].x += a1[idy + 1].x;

		a1[idy].y += a1[idy + 32].y;
		a1[idy].y += a1[idy + 16].y;
		a1[idy].y += a1[idy + 8].y;
		a1[idy].y += a1[idy + 4].y;
		a1[idy].y += a1[idy + 2].y;
		a1[idy].y += a1[idy + 1].y;

		a1[idy].z += a1[idy + 32].z;
		a1[idy].z += a1[idy + 16].z;
		a1[idy].z += a1[idy + 8].z;
		a1[idy].z += a1[idy + 4].z;
		a1[idy].z += a1[idy + 2].z;
		a1[idy].z += a1[idy + 1].z;

	}

	else{
		if(idy < 64){
			a2[idy-32].x += a2[idy + 32-32].x;
			a2[idy-32].x += a2[idy + 16-32].x;
			a2[idy-32].x += a2[idy + 8-32].x;
			a2[idy-32].x += a2[idy + 4-32].x;
			a2[idy-32].x += a2[idy + 2-32].x;
			a2[idy-32].x += a2[idy + 1-32].x;

			a2[idy-32].y += a2[idy + 32-32].y;
			a2[idy-32].y += a2[idy + 16-32].y;
			a2[idy-32].y += a2[idy + 8-32].y;
			a2[idy-32].y += a2[idy + 4-32].y;
			a2[idy-32].y += a2[idy + 2-32].y;
			a2[idy-32].y += a2[idy + 1-32].y;
	
			a2[idy-32].z += a2[idy + 32-32].z;
			a2[idy-32].z += a2[idy + 16-32].z;
			a2[idy-32].z += a2[idy + 8-32].z;
			a2[idy-32].z += a2[idy + 4-32].z;
			a2[idy-32].z += a2[idy + 2-32].z;
			a2[idy-32].z += a2[idy + 1-32].z;	

		}
		else{
			if(idy < 96){
				a3[idy-64].x += a3[idy + 32-64].x;
				a3[idy-64].x += a3[idy + 16-64].x;
				a3[idy-64].x += a3[idy + 8-64].x;
				a3[idy-64].x += a3[idy + 4-64].x;
				a3[idy-64].x += a3[idy + 2-64].x;
				a3[idy-64].x += a3[idy + 1-64].x;
				
				a3[idy-64].y += a3[idy + 32-64].y;
				a3[idy-64].y += a3[idy + 16-64].y;
				a3[idy-64].y += a3[idy + 8-64].y;
				a3[idy-64].y += a3[idy + 4-64].y;
				a3[idy-64].y += a3[idy + 2-64].y;
				a3[idy-64].y += a3[idy + 1-64].y;
				
				a3[idy-64].z += a3[idy + 32-64].z;
				a3[idy-64].z += a3[idy + 16-64].z;
				a3[idy-64].z += a3[idy + 8-64].z;
				a3[idy-64].z += a3[idy + 4-64].z;
				a3[idy-64].z += a3[idy + 2-64].z;
				a3[idy-64].z += a3[idy + 1-64].z;

			}
			else{
				if(idy < 128){
					a4[idy-96].x += a4[idy + 32-96].x;
					a4[idy-96].x += a4[idy + 16-96].x;
					a4[idy-96].x += a4[idy + 8-96].x;
					a4[idy-96].x += a4[idy + 4-96].x;
					a4[idy-96].x += a4[idy + 2-96].x;
					a4[idy-96].x += a4[idy + 1-96].x;

					a4[idy-96].y += a4[idy + 32-96].y;
					a4[idy-96].y += a4[idy + 16-96].y;
					a4[idy-96].y += a4[idy + 8-96].y;
					a4[idy-96].y += a4[idy + 4-96].y;
					a4[idy-96].y += a4[idy + 2-96].y;
					a4[idy-96].y += a4[idy + 1-96].y;

					a4[idy-96].z += a4[idy + 32-96].z;
					a4[idy-96].z += a4[idy + 16-96].z;
					a4[idy-96].z += a4[idy + 8-96].z;
					a4[idy-96].z += a4[idy + 4-96].z;
					a4[idy-96].z += a4[idy + 2-96].z;
					a4[idy-96].z += a4[idy + 1-96].z;

				}
			}
		}
	}
	__syncthreads();

	if(idy == 0){
		v4_d[idx].x += a1[0].x;
		v4_d[idx].y += a1[0].y;
		v4_d[idx].z += a1[0].z;
	}

	if(idy == 32){
		v4_d[idx + N4].x += a2[0].x;
		v4_d[idx + N4].y += a2[0].y;
		v4_d[idx + N4].z += a2[0].z;
	}

	if(idy == 64){
			v4_d[idx + 2*N4].x += a3[0].x;
			v4_d[idx + 2*N4].y += a3[0].y;
			v4_d[idx + 2*N4].z += a3[0].z;
	}
	
	if(idy == 96){
			v4_d[idx + 3*N4].x += a4[0].x;
			v4_d[idx + 3*N4].y += a4[0].y;
			v4_d[idx + 3*N4].z += a4[0].z;
	}

}

template <int Bl>
__global__ void kick4_kernel(T4 *x4_d, T4 *v4_d, int N, int N4){
	int idy = threadIdx.x;
	int idx = blockIdx.x;

	int Bl_2 = Bl/2;

	__shared__ T3 a1_s[Bl/2];
	__shared__ T3 a2_s[Bl/2];
	__shared__ T3 a3_s[Bl/2];
	__shared__ T3 a4_s[Bl/2];

	T4 x4i = x4_d[idx];
	T4 x4i2 = x4_d[idx+N4];
	T4 x4i3 = x4_d[idx+2*N4];
	T4 x4i4 = x4_d[idx+3*N4];


	if(idy < Bl_2) {
		a1_s[idy].x = 0.0;
		a1_s[idy].y = 0.0;
		a1_s[idy].z = 0.0;

		a3_s[idy].x = 0.0;
		a3_s[idy].y = 0.0;
		a3_s[idy].z = 0.0;

		__syncthreads();
		for(int i = 0; i < N; i += Bl_2){
			if(idy + i < N){
				T4 x4j = x4_d[idy + i];
				if(idx < N)        accG(a1_s[idy], x4i, x4j);
				if(idx + 2*N4 < N) accG(a3_s[idy], x4i3, x4j);
			}	
		}
	}
	else{
		a2_s[idy-Bl_2].x = 0.0;
		a2_s[idy-Bl_2].y = 0.0;
		a2_s[idy-Bl_2].z = 0.0;

		a4_s[idy-Bl_2].x = 0.0;
		a4_s[idy-Bl_2].y = 0.0;
		a4_s[idy-Bl_2].z = 0.0;
		__syncthreads();

		for(int i = 0; i < N; i += Bl_2){
			if(idy-Bl_2 + i < N){
				T4 x4j = x4_d[idy-Bl_2 + i];
				if(idx + N4 < N)   accG(a2_s[idy-Bl_2], x4i2, x4j);
				if(idx + 3*N4 < N) accG(a4_s[idy-Bl_2], x4i4, x4j);
			}
		}
	}
	__syncthreads();

	volatile T3 *a1 = a1_s;
	volatile T3 *a2 = a2_s;
	volatile T3 *a3 = a3_s;
	volatile T3 *a4 = a4_s;

	int s = Bl/4;

	for(int i = 6; i < log2f(Bl/2); ++i){
		if( idy < s ) {
			a1[idy].x += a1[idy + s].x;
			a1[idy].y += a1[idy + s].y;
			a1[idy].z += a1[idy + s].z;

			a2[idy].x += a2[idy + s].x;
			a2[idy].y += a2[idy + s].y;
			a2[idy].z += a2[idy + s].z;

			a3[idy].x += a3[idy + s].x;
			a3[idy].y += a3[idy + s].y;
			a3[idy].z += a3[idy + s].z;

			a4[idy].x += a4[idy + s].x;
			a4[idy].y += a4[idy + s].y;
			a4[idy].z += a4[idy + s].z;

		}
		__syncthreads();
		s /= 2;
	}

	if(idy < 32){
		a1[idy].x += a1[idy + 32].x;
		a1[idy].x += a1[idy + 16].x;
		a1[idy].x += a1[idy + 8].x;
		a1[idy].x += a1[idy + 4].x;
		a1[idy].x += a1[idy + 2].x;
		a1[idy].x += a1[idy + 1].x;

		a1[idy].y += a1[idy + 32].y;
		a1[idy].y += a1[idy + 16].y;
		a1[idy].y += a1[idy + 8].y;
		a1[idy].y += a1[idy + 4].y;
		a1[idy].y += a1[idy + 2].y;
		a1[idy].y += a1[idy + 1].y;

		a1[idy].z += a1[idy + 32].z;
		a1[idy].z += a1[idy + 16].z;
		a1[idy].z += a1[idy + 8].z;
		a1[idy].z += a1[idy + 4].z;
		a1[idy].z += a1[idy + 2].z;
		a1[idy].z += a1[idy + 1].z;

	}
	else{
		if(idy < 64){
			a2[idy-32].x += a2[idy + 32-32].x;
			a2[idy-32].x += a2[idy + 16-32].x;
			a2[idy-32].x += a2[idy + 8-32].x;
			a2[idy-32].x += a2[idy + 4-32].x;
			a2[idy-32].x += a2[idy + 2-32].x;
			a2[idy-32].x += a2[idy + 1-32].x;

			a2[idy-32].y += a2[idy + 32-32].y;
			a2[idy-32].y += a2[idy + 16-32].y;
			a2[idy-32].y += a2[idy + 8-32].y;
			a2[idy-32].y += a2[idy + 4-32].y;
			a2[idy-32].y += a2[idy + 2-32].y;
			a2[idy-32].y += a2[idy + 1-32].y;

			a2[idy-32].z += a2[idy + 32-32].z;
			a2[idy-32].z += a2[idy + 16-32].z;
			a2[idy-32].z += a2[idy + 8-32].z;
			a2[idy-32].z += a2[idy + 4-32].z;
			a2[idy-32].z += a2[idy + 2-32].z;
			a2[idy-32].z += a2[idy + 1-32].z;

		}
		else{
			if(idy < 96){
				a3[idy-64].x += a3[idy + 32-64].x;
				a3[idy-64].x += a3[idy + 16-64].x;
				a3[idy-64].x += a3[idy + 8-64].x;
				a3[idy-64].x += a3[idy + 4-64].x;
				a3[idy-64].x += a3[idy + 2-64].x;
				a3[idy-64].x += a3[idy + 1-64].x;

				a3[idy-64].y += a3[idy + 32-64].y;
				a3[idy-64].y += a3[idy + 16-64].y;
				a3[idy-64].y += a3[idy + 8-64].y;
				a3[idy-64].y += a3[idy + 4-64].y;
				a3[idy-64].y += a3[idy + 2-64].y;
				a3[idy-64].y += a3[idy + 1-64].y;

				a3[idy-64].z += a3[idy + 32-64].z;
				a3[idy-64].z += a3[idy + 16-64].z;
				a3[idy-64].z += a3[idy + 8-64].z;
				a3[idy-64].z += a3[idy + 4-64].z;
				a3[idy-64].z += a3[idy + 2-64].z;
				a3[idy-64].z += a3[idy + 1-64].z;

			}
			else{
				if(idy < 128){
					a4[idy-96].x += a4[idy + 32-96].x;
					a4[idy-96].x += a4[idy + 16-96].x;
					a4[idy-96].x += a4[idy + 8-96].x;
					a4[idy-96].x += a4[idy + 4-96].x;
					a4[idy-96].x += a4[idy + 2-96].x;
					a4[idy-96].x += a4[idy + 1-96].x;

					a4[idy-96].y += a4[idy + 32-96].y;
					a4[idy-96].y += a4[idy + 16-96].y;
					a4[idy-96].y += a4[idy + 8-96].y;
					a4[idy-96].y += a4[idy + 4-96].y;
					a4[idy-96].y += a4[idy + 2-96].y;
					a4[idy-96].y += a4[idy + 1-96].y;

					a4[idy-96].z += a4[idy + 32-96].z;
					a4[idy-96].z += a4[idy + 16-96].z;
					a4[idy-96].z += a4[idy + 8-96].z;
					a4[idy-96].z += a4[idy + 4-96].z;
					a4[idy-96].z += a4[idy + 2-96].z;
					a4[idy-96].z += a4[idy + 1-96].z;

				}
			}
		}
	}


	__syncthreads();

	if(idy == 0  && idx < N){
		v4_d[idx].x += a1[0].x;
		v4_d[idx].y += a1[0].y;
		v4_d[idx].z += a1[0].z;
	}
	if(idy == 32  && idx + N4< N){
		v4_d[idx + N4].x += a2[0].x;
		v4_d[idx + N4].y += a2[0].y;
		v4_d[idx + N4].z += a2[0].z;
	}
	if(idy == 64  && idx + 2*N4  < N){
		v4_d[idx + 2*N4].x += a3[0].x;
		v4_d[idx + 2*N4].y += a3[0].y;
		v4_d[idx + 2*N4].z += a3[0].z;
	}
	if(idy == 96  && idx + 3*N4< N){
		v4_d[idx + 3*N4].x += a4[0].x;
		v4_d[idx + 3*N4].y += a4[0].y;
		v4_d[idx + 3*N4].z += a4[0].z;
	}
}


template <const int Bl, const int p>
__global__ void kick4B_kernel(T4 *x4_d, T4 *v4_d, int Nstart, int Nend, int N){

	int idy = threadIdx.x ;
	int idx = (blockIdx.x * blockDim.y + threadIdx.y) * p + Nstart;


	__shared__ T3 a_s[Bl * p];
	__shared__ T4 x4_s[p];

	if(idy < p){
		x4_s[idy] = x4_d[idx + idy];
	}


	if(idy < Bl) {

		for(int j = 0; j < p; ++j){
			a_s[idy + j* Bl].x = 0.0;
			a_s[idy + j *Bl].y = 0.0;
			a_s[idy + j *Bl].z = 0.0;
		}

		__syncthreads();
		for(int i = 0; i < N; i += Bl){
			if(idy + i < N){
				T4 x4j = x4_d[idy + i];
				for(int j = 0; j < p; ++j){
					if(idx + j < Nend)     accG(a_s[idy + j * Bl], x4_s[j], x4j);
				}
			}	
		}
	}
	__syncthreads();

	
	int s = Bl/2;
	
	for(int i = 6; i < log2f(Bl); ++i){
		if( idy < s ) {
			for(int j = 0; j < p; ++j){
				volatile T3 *a = a_s + j * Bl;
				a[idy].x += a[idy + s].x;
				a[idy].y += a[idy + s].y;
				a[idy].z += a[idy + s].z;
			}
		}
		__syncthreads();
		s /= 2;
	}
	

	if(idy < min(32, Bl / 2)){

		for(int j = 0; j < p; ++j){
			volatile T3 *a = a_s + j * Bl;

			if(Bl > 32) a[idy].x += a[idy + 32].x;
			if(Bl > 16) a[idy].x += a[idy + 16].x;
			a[idy].x += a[idy + 8].x;
			a[idy].x += a[idy + 4].x;
			a[idy].x += a[idy + 2].x;
			a[idy].x += a[idy + 1].x;

			if(Bl > 32) a[idy].y += a[idy + 32].y;
			if(Bl > 16) a[idy].y += a[idy + 16].y;
			a[idy].y += a[idy + 8].y;
			a[idy].y += a[idy + 4].y;
			a[idy].y += a[idy + 2].y;
			a[idy].y += a[idy + 1].y;

			if(Bl > 32) a[idy].z += a[idy + 32].z;
			if(Bl > 16) a[idy].z += a[idy + 16].z;
			a[idy].z += a[idy + 8].z;
			a[idy].z += a[idy + 4].z;
			a[idy].z += a[idy + 2].z;
			a[idy].z += a[idy + 1].z;
		}
	}


	__syncthreads();

	if(idy < p){
		if(idx + idy < Nend){
			v4_d[idx].x += a_s[idy * Bl].x;
			v4_d[idx].y += a_s[idy * Bl].y;
			v4_d[idx].z += a_s[idy * Bl].z;
		}
	}
}

