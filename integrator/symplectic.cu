#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "fgc.h"

#define dayUnit 0.01720209895

//nvcc -arch sm_52 -o symplecticcuda symplectic.cu

__device__ void  acc(double3 &ac, double4 &x4i, double4 &x4j, int j, int i){
	double rsq, ir, ir3, s;
	double3 r3ij;


	r3ij.x = x4j.x - x4i.x;
	r3ij.y = x4j.y - x4i.y;
	r3ij.z = x4j.z - x4i.z;

	rsq = (r3ij.x*r3ij.x) + (r3ij.y*r3ij.y) + (r3ij.z*r3ij.z);

	ir = 1.0/sqrt(rsq);
	ir3 = ir*ir*ir;

	s = (i != j)? (x4j.w * ir3) : 0.0;

	ac.x += __dmul_rn(r3ij.x, s);
	ac.y += __dmul_rn(r3ij.y, s);
	ac.z += __dmul_rn(r3ij.z, s);
}

__global__ void step_kernel(double4 *x4_d, double4 *v4_d, const double dt, const double m0, const int GR, const int N, const int nsteps){

	int id = threadIdx.x;
	int idy = blockIdx.x;

	if(id < N){

		double4 x4 = x4_d[idy * N + id];
		double4 v4 = v4_d[idy * N + id];

	
		double mu = m0;
		
		for(int t = 0; t < nsteps; ++t){

			//Kick
			double3 a = {0.0, 0.0, 0.0};
			for(int j = 0; j < N; ++j){
				double4 x4j;
				x4j.x = __shfl_sync(0xffffffff, x4.x, j, 32);
				x4j.y = __shfl_sync(0xffffffff, x4.y, j, 32);
				x4j.z = __shfl_sync(0xffffffff, x4.z, j, 32);
				x4j.w = __shfl_sync(0xffffffff, x4.w, j, 32);
//printf("Kick %d %d %g %g\n", id, j, x4.x, x4j.x);
				acc(a, x4, x4j, j, id);
			}

			v4.x += __dmul_rn(a.x, dt * 0.5);
			v4.y += __dmul_rn(a.y, dt * 0.5);
			v4.z += __dmul_rn(a.z, dt * 0.5);

			//HC
			a = {0.0, 0.0, 0.0};
			for(int j = 0; j < N; ++j){
				double4 v4j;
				double mj;
				v4j.x = __shfl_sync(0xffffffff, v4.x, j, 32);
				v4j.y = __shfl_sync(0xffffffff, v4.y, j, 32);
				v4j.z = __shfl_sync(0xffffffff, v4.z, j, 32);
				mj =    __shfl_sync(0xffffffff, x4.w, j, 32);
				a.x += mj * v4j.x;
				a.y += mj * v4j.y;
				a.z += mj * v4j.z;
//printf("HCA %d %d %g %g %g %g\n", id, j, v4.x, v4j.x, mj, a.x);
			}

			x4.x += __dmul_rn(a.x, dt * 0.5 / m0);
			x4.y += __dmul_rn(a.y, dt * 0.5 / m0);
			x4.z += __dmul_rn(a.z, dt * 0.5 / m0);


			//FG
			fgcfull(x4, v4, dt, mu, GR);


			//HC
			a = {0.0, 0.0, 0.0};
			for(int j = 0; j < N; ++j){
				double4 v4j;
				double mj;
				v4j.x = __shfl_sync(0xffffffff, v4.x, j, 32);
				v4j.y = __shfl_sync(0xffffffff, v4.y, j, 32);
				v4j.z = __shfl_sync(0xffffffff, v4.z, j, 32);
				mj =    __shfl_sync(0xffffffff, x4.w, j, 32);
				a.x += mj * v4j.x;
				a.y += mj * v4j.y;
				a.z += mj * v4j.z;
//printf("HCA %d %d %g %g %g %g\n", id, j, v4.x, v4j.x, mj, a.x);
			}

			x4.x += __dmul_rn(a.x, dt * 0.5 / m0);
			x4.y += __dmul_rn(a.y, dt * 0.5 / m0);
			x4.z += __dmul_rn(a.z, dt * 0.5 / m0);

			//Kick
			a = {0.0, 0.0, 0.0};
			for(int j = 0; j < N; ++j){
				double4 x4j;
				x4j.x = __shfl_sync(0xffffffff, x4.x, j, 32);
				x4j.y = __shfl_sync(0xffffffff, x4.y, j, 32);
				x4j.z = __shfl_sync(0xffffffff, x4.z, j, 32);
				x4j.w = __shfl_sync(0xffffffff, x4.w, j, 32);
//printf("Kick %d %d %g %g\n", id, j, x4.x, x4j.x);
				acc(a, x4, x4j, j, id);
			}

			v4.x += __dmul_rn(a.x, dt * 0.5);
			v4.y += __dmul_rn(a.y, dt * 0.5);
			v4.z += __dmul_rn(a.z, dt * 0.5);
		}
		x4_d[idy * N + id].x = x4.x;
		x4_d[idy * N + id].y = x4.y;
		x4_d[idy * N + id].z = x4.z;
		x4_d[idy * N + id].w = x4.w;
		v4_d[idy * N + id].x = v4.x;
		v4_d[idy * N + id].y = v4.y;
		v4_d[idy * N + id].z = v4.z;

	}
}



int main(int argc, char **argv){

	//Number of planets
	int N = 7;

	int printHelioCentric = 1;	//1: heliocentric coordinates, 0: barycentric coordinates

	/*		
	int Nst = 625; //Number of simulations
	double dt = 0.06 * dayUnit;
	long long int Nsteps = 61000000000ll;
	long long int nsteps = 100000ll;
	long long int OutInterval = 10000000ll;
	double time0 = 7257.93115525;
	int timeUnits = 1;		//1 print days, 0 print years	
	*/

		
	int Nst = 1; //Number of simulations
	double dt = 0.06 * dayUnit;
	long long int Nsteps = 100000ll;
	long long int nsteps = 1000ll;
	long long int OutInterval = 1000ll;
	double time0 = 0.0;
	int timeUnits = 0;		//1 print days, 0 print years	
	

	double m0 = 0.09;

	//Read console input arguments
	for(int i = 1; i < argc; i += 2){
		if(strcmp(argv[i], "-Nst") == 0){
			Nst = atoi(argv[i + 1]);
		}
	}


	double tscale = 1.0;
	if(timeUnits == 0) tscale = 1.0/365.25;

	double4 *x4_h, *v4_h;
	double4 *x4_d, *v4_d;

	//allocate data on host
	x4_h = (double4*)malloc(N * Nst * sizeof(double4));
	v4_h = (double4*)malloc(N * Nst * sizeof(double4));

	//allocate data on the device
	cudaMalloc((void **) &x4_d, N * Nst * sizeof(double4));
	cudaMalloc((void **) &v4_d, N * Nst * sizeof(double4));

	FILE *outfile;
	char outfilename[160];	

	FILE *infile;
	char infilename[160];
	

	//read initial file
	sprintf(infilename, "initial7.dat");
	infile = fopen(infilename, "r");
	for(int s = 0; s < Nst; ++s){
		for(int i = 0; i < N; ++i){
			double skipd;
			fscanf(infile, "%lf", &skipd);
			fscanf(infile, "%lf", &x4_h[s * N + i].w);
			fscanf(infile, "%lf", &v4_h[s * N + i].w);
			fscanf(infile, "%lf", &x4_h[s * N + i].x);
			fscanf(infile, "%lf", &x4_h[s * N + i].y);
			fscanf(infile, "%lf", &x4_h[s * N + i].z);
			fscanf(infile, "%lf", &v4_h[s * N + i].x);
			fscanf(infile, "%lf", &v4_h[s * N + i].y);
			fscanf(infile, "%lf", &v4_h[s * N + i].z);

		}
	}
	fclose(infile);

	//democratic
	//convert to barycentric velocities

	for(int s = 0; s < Nst; ++s){
		double vcomx = 0.0;
		double vcomy = 0.0;
		double vcomz = 0.0;
		double mtot = m0;

		for(int i = 0; i < N; ++i){
			vcomx += x4_h[s * N + i].w * v4_h[s * N + i].x;
			vcomy += x4_h[s * N + i].w * v4_h[s * N + i].y;
			vcomz += x4_h[s * N + i].w * v4_h[s * N + i].z;
			mtot += x4_h[s * N + i].w;
		}

		for(int i = 0; i < N; ++i){
			v4_h[s * N + i].x -= vcomx / mtot;
			v4_h[s * N + i].y -= vcomy / mtot;
			v4_h[s * N + i].z -= vcomz / mtot;
		}
	}



	//copy the data to the device
	cudaMemcpy(x4_d, x4_h, N * Nst * sizeof(double4), cudaMemcpyHostToDevice);	
	cudaMemcpy(v4_d, v4_h, N * Nst * sizeof(double4), cudaMemcpyHostToDevice);	

	//printf initial time step
	for(int s = 0; s < Nst; ++s){

		if(printHelioCentric == 1){
			//convert to heliocentric velocities

			double vcomx = 0.0;
			double vcomy = 0.0;
			double vcomz = 0.0;

			for(int i = 0; i < N; ++i){
				vcomx += x4_h[s * N + i].w * v4_h[s * N + i].x;
				vcomy += x4_h[s * N + i].w * v4_h[s * N + i].y;
				vcomz += x4_h[s * N + i].w * v4_h[s * N + i].z;
			}

			//sprintf(outfilename, "OutM_%.4d_%.12lld.dat", s, 0ll);
			//outfile = fopen(outfilename, "w");
			sprintf(outfilename, "OutMM%.4d.dat", s);
			outfile = fopen(outfilename, "a");
if(s == 0) printf("%s\n", outfilename);
			for(int i = 0; i < N; ++i){
				double m = x4_h[s * N + i].w;
				double rad = v4_h[s * N + i].w;
				double xh = x4_h[s * N + i].x;
				double yh = x4_h[s * N + i].y;
				double zh = x4_h[s * N + i].z;
				double vxh = v4_h[s * N + i].x + vcomx / m0;
				double vyh = v4_h[s * N + i].y + vcomy / m0;
				double vzh = v4_h[s * N + i].z + vcomz / m0;

				fprintf(outfile, "%.10g %d %.20g %.20g %.20g %.20g %.20g %.20g %.20g %.20g 0 0 0 0 0 0 0 0 0 0 0\n",  time0 * tscale, i, m, rad, xh, yh, zh, vxh, vyh, vzh);
			}
		}
		else{
			//convert to barycentric positions

			double comx = 0.0;
			double comy = 0.0;
			double comz = 0.0;
			double mtot = m0;

			for(int i = 0; i < N; ++i){
				comx += x4_h[s * N + i].w * x4_h[s * N + i].x;
				comy += x4_h[s * N + i].w * x4_h[s * N + i].y;
				comz += x4_h[s * N + i].w * x4_h[s * N + i].z;
				mtot += x4_h[s * N + i].w;
			}

			sprintf(outfilename, "OutM_%.4d_%.12lld.dat", s, 0ll);
			outfile = fopen(outfilename, "w");
if(s == 0) printf("%s\n", outfilename);
			for(int i = 0; i < N; ++i){
				double m = x4_h[s * N + i].w;
				double rad = v4_h[s * N + i].w;
				double xb = x4_h[s * N + i].x - comx / mtot;
				double yb = x4_h[s * N + i].y - comy / mtot;
				double zb = x4_h[s * N + i].z - comz / mtot;
				double vxb = v4_h[s * N + i].x;
				double vyb = v4_h[s * N + i].y;
				double vzb = v4_h[s * N + i].z;

				fprintf(outfile, "%.10g %d %.20g %.20g %.20g %.20g %.20g %.20g %.20g %.20g 0 0 0 0 0 0 0 0 0 0 0\n",  time0 * tscale, i, m, rad, xb, yb, zb, vxb, vyb, vzb);
			}
			
			fclose(outfile);
		}
	}
	int GR = 0;

	cudaDeviceSynchronize();
	for(long long int t = 1; t <= Nsteps / nsteps; ++t){

		step_kernel <<< Nst, N >>> (x4_d, v4_d, dt, m0, GR, N, nsteps);

		if((t * nsteps) % OutInterval == 0){
			//copy the data to the host
			cudaMemcpy(x4_h, x4_d, N * Nst * sizeof(double4), cudaMemcpyDeviceToHost);	
			cudaMemcpy(v4_h, v4_d, N * Nst * sizeof(double4), cudaMemcpyDeviceToHost);	

			for(int s = 0; s < Nst; ++s){

				if(printHelioCentric == 1){
					//convert to heliocentric velocities

					double vcomx = 0.0;
					double vcomy = 0.0;
					double vcomz = 0.0;

					for(int i = 0; i < N; ++i){
						vcomx += x4_h[s * N + i].w * v4_h[s * N + i].x;
						vcomy += x4_h[s * N + i].w * v4_h[s * N + i].y;
						vcomz += x4_h[s * N + i].w * v4_h[s * N + i].z;
					}

					//sprintf(outfilename, "OutM_%.4d_%.12lld.dat", s, t * nsteps);
					//outfile = fopen(outfilename, "w");
					sprintf(outfilename, "OutMM%.4d.dat", s);
					outfile = fopen(outfilename, "a");
if(s == 0) printf("%s %lld\n", outfilename, t * nsteps);
					for(int i = 0; i < N; ++i){
						double m = x4_h[s * N + i].w;
						double rad = v4_h[s * N + i].w;
						double xh = x4_h[s * N + i].x;
						double yh = x4_h[s * N + i].y;
						double zh = x4_h[s * N + i].z;
						double vxh = v4_h[s * N + i].x + vcomx / m0;
						double vyh = v4_h[s * N + i].y + vcomy / m0;
						double vzh = v4_h[s * N + i].z + vcomz / m0;

						fprintf(outfile, "%.10g %d %.20g %.20g %.20g %.20g %.20g %.20g %.20g %.20g 0 0 0 0 0 0 0 0 0 0 0\n", (time0 + t * nsteps * dt / dayUnit) * tscale, i, m, rad, xh, yh, zh, vxh, vyh, vzh);
					}
				}
				else{	
					//convert to barycentric positions
					

					double comx = 0.0;
					double comy = 0.0;
					double comz = 0.0;
					double mtot = m0;

					for(int i = 0; i < N; ++i){
						comx += x4_h[s * N + i].w * x4_h[s * N + i].x;
						comy += x4_h[s * N + i].w * x4_h[s * N + i].y;
						comz += x4_h[s * N + i].w * x4_h[s * N + i].z;
						mtot += x4_h[s * N + i].w;
					}

					//sprintf(outfilename, "OutM_%.4d_%.12lld.dat", s, t * nsteps);
					//outfile = fopen(outfilename, "w");
					sprintf(outfilename, "OutMM%.4d.dat", s);
					outfile = fopen(outfilename, "a");
if(s == 0) printf("%s\n", outfilename);
					for(int i = 0; i < N; ++i){
						double m = x4_h[s * N + i].w;
						double rad = v4_h[s * N + i].w;
						double xb = x4_h[s * N + i].x - comx / mtot;
						double yb = x4_h[s * N + i].y - comy / mtot;
						double zb = x4_h[s * N + i].z - comz / mtot;
						double vxb = v4_h[s * N + i].x;
						double vyb = v4_h[s * N + i].y;
						double vzb = v4_h[s * N + i].z;

						fprintf(outfile, "%.10g %d %.20g %.20g %.20g %.20g %.20g %.20g %.20g %.20g 0 0 0 0 0 0 0 0 0 0 0\n", (time0 + t * nsteps * dt / dayUnit) * tscale, i, m, rad, xb, yb, zb, vxb, vyb, vzb);
					}
					fclose(outfile);
				}
			}
		}
	}

	cudaDeviceSynchronize();
	printf("End\n");

}
