__device__ void fgcfull(double4 &x4, double4 &v4, double dt, double mu, int GR){

	double dec;                                      /* delta E */
	double wp;
	double ria;
	double en;
	double c, s;
	double converge;
	double UP = 2*M_PI;
	double LOW = -2*M_PI;
	int i;
	/*
	* Evaluate some orbital quantites.
	*/

	double rsq = x4.x*x4.x + x4.y*x4.y + x4.z*x4.z;
	double vsq = v4.x*v4.x + v4.y*v4.y + v4.z*v4.z;
	double u   = x4.x*v4.x + x4.y*v4.y + x4.z*v4.z;
	double ir = 1.0 / sqrt(rsq);
	double ia = 2.0*ir-vsq/mu;

/*	
	if(GR == 1){// GR time rescale (Saha & Tremaine 1994)
		double c = 10065.3201686;//c in AU / day * 0.0172020989
		double c2 = c * c;
		dt *= 1.0 - 1.5 * mu * ia / c2;
	}
*/	
//printf("%g %g %g %g %g %g\n", rsq, vsq, u, ir, ia, mu);

	if(ia > 0){

		double t1 = ia*ia;
		ria = rsq*ir*ia;
		en = sqrt(mu*t1*ia);
		double ec = 1.0-ria;
		double es = u*t1/en;
		double e = sqrt(ec*ec + es*es);
		double dm = en * dt - es;

		sincos(dm, &s, &c);
		double ff = double((es*c+ec*s) > 0);
		dec = dm + 0.85*e * ff - 0.85*e*(1.0-ff);

		converge = fabs(en * dt * 1.2e-16);

		for(i = 0; i < 128; ++i) {

			sincos(dec, &s, &c);
			double wpp = ec*s + es*c;
			double wppp = ec*c - es*s;
			double mw = dm - dec + wpp;
//printf("mw %.10g %.10g %.10g\n", mw, dec, wpp);
			if(mw < 0.0){
				UP = dec;
			}
			else LOW = dec;
			wp = 1.0 - wppp;
			wpp *= 0.5;
			double dx = mw/wp;
			dx = mw/(wp + dx*wpp);
			dx = mw/(wp + dx*(wpp + (1.0/6.0)*dx*wppp));
			double next = dec + dx;
//printf("dx %d %.20g %g %g %g %.20g\n", i, dx, wp, mw, dm, dec);
			if (fabs(dx) <= converge) break;
			if(next > LOW && next < UP){
				dec = next;
			}
			else dec = 0.5*(LOW + UP);
			if (dec==LOW || dec==UP) break;
		}
	}
	__syncthreads();

	if(ia > 0){
		if(i < 127){
			double iwp = 1.0/wp;
			double air_ = -1.0/ria;
			double t1 = (1.0-c);
			double f = 1.0 + air_*t1;
			double g = dt + (s-dec)/en;
			double fd = air_*iwp*s*en;
			double gd = 1.0 - iwp*t1;

			double tx = f*x4.x+g*v4.x;
			double ty = f*x4.y+g*v4.y;
			double tz = f*x4.z+g*v4.z;
			v4.x = fd*x4.x+gd*v4.x;
			v4.y = fd*x4.y+gd*v4.y;
			v4.z = fd*x4.z+gd*v4.z;
			x4.x = tx;
			x4.y = ty;
			x4.z = tz;
//printf("xv %g %g\n", x4.x, v4.x);
		}
		else{
printf("not converged\n");
		}

	}
	else{
printf("Negative a %g %g\n", ia, 1.0/ia);
	x4.w = 0.0;
	}

}
