#include "hip/hip_runtime.h"
#include <stdio.h>
#include <stdlib.h>

//nvcc -o test00 test00.cu


__global__ void kernel(double *a_d){

	int idx = blockDim.x * blockIdx.x + threadIdx.x;

	a_d[idx] = idx;
	printf("hello from thread %d\n", idx);
	
}


int main(){

	#ifdef __NVCC__
	printf("nvcc\n");
	#endif

	#ifdef     __HIP_PLATFORM_NVCC__
	printf("hip nvcc platform\n");
	#endif

	double *a_h, *a_d;

	const int N = 32;

	a_h = (double*)malloc(N * sizeof(double));

	hipMalloc((void**)&a_d, N * sizeof(double));


	for(int i = 0; i < N; ++i){

		a_h[i] = 0.0;
	}

	hipMemcpy(a_d, a_h, N * sizeof(double), hipMemcpyHostToDevice);

	hipLaunchKernelGGL(kernel, dim3(1), dim3(N ), 0, 0, a_d);

	hipMemcpy(a_h, a_d, N * sizeof(double), hipMemcpyDeviceToHost);

	for(int i = 0; i < N; ++i){

		printf("%d %g\n", i, a_h[i]);
	}

	free(a_h);
	hipFree(a_d);

	return 0;

}

