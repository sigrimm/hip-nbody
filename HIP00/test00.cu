#include <stdio.h>
#include <stdlib.h>

//nvcc -o test00 test00.cu


__global__ void kernel(double *a_d){

	int idx = blockDim.x * blockIdx.x + threadIdx.x;

	a_d[idx] = idx;
	printf("hello from thread %d\n", idx);
	
}


int main(){

	#ifdef __NVCC__
	printf("nvcc\n");
	#endif

	#ifdef     __HIP_PLATFORM_NVCC__
	printf("hip nvcc platform\n");
	#endif

	double *a_h, *a_d;

	const int N = 32;

	a_h = (double*)malloc(N * sizeof(double));

	cudaMalloc((void**)&a_d, N * sizeof(double));


	for(int i = 0; i < N; ++i){

		a_h[i] = 0.0;
	}

	cudaMemcpy(a_d, a_h, N * sizeof(double), cudaMemcpyHostToDevice);

	kernel <<< 1, N >>> (a_d);

	cudaMemcpy(a_h, a_d, N * sizeof(double), cudaMemcpyDeviceToHost);

	for(int i = 0; i < N; ++i){

		printf("%d %g\n", i, a_h[i]);
	}

	free(a_h);
	cudaFree(a_d);

	return 0;

}

