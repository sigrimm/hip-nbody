dmesg | head -1
[    0.000000] Linux version 5.3.0-59-generic (buildd@lcy01-amd64-025) (gcc version 7.5.0 (Ubuntu 7.5.0-3ubuntu1~18.04)) #53~18.04.1-Ubuntu SMP Thu Jun 4 14:58:26 UTC 2020 (Ubuntu 5.3.0-59.53~18.04.1-generic 5.3.18)


https://rocmdocs.amd.com/en/latest/Installation_Guide/Installation-Guide.html

install CUDA first
sudo apt install libnuma-dev
sudo reboot


sudo apt install wget gnupg2

--------------------
uninstall rocm

sudo apt autoremove rocm-opencl rocm-dkms rocm-dev rocm-utils && sudo reboot
---------------------




wget -q -O - https://repo.radeon.com/rocm/rocm.gpg.key | sudo apt-key add -

echo 'deb [arch=amd64] https://repo.radeon.com/rocm/apt/debian/ ubuntu main' | sudo tee /etc/apt/sources.list.d/rocm.list


sudo apt install rocm-dkms
sudo reboot

echo 'export PATH=$PATH:/opt/rocm/bin:/opt/rocm/rocprofiler/bin:/opt/rocm/opencl/bin' | sudo tee -a /etc/profile.d/rocm.sh

HIP
sudo apt install mesa-common-dev
sudo apt install clang
sudo apt install comgr
sudo apt-get -y install rocm-dkms

The rocm symlink is wrong, change to
(sudo ln -sfn rocm-4.1.0/ rocm) ?

export ROCM_PATH=/opt/rocm-4.0.0




HIP on Nvidia system
apt-get install hip-nvcc
get error message but seems to be working still

check:
/opt/rocm/bin/hipconfig --full



rocm-smi


choose nvcc or amd
export HIP_PLATFORM=nvidia
export HIP_PLATFORM=amd



-------------------------
install hipify

sudo apt install libclang-dev
sudo apt install libclanlib-dev
sudo apt install libclang-dev

git clone https://github.com/ROCm-Developer-Tools/HIPIFY.git

cd HIPIFY/
mkdir build dist
cd build/
cmake -DCMAKE_INSTALL_PREFIX=../dist -DCMAKE_BUILD_TYPE=Release ..
make -j install




use with:
hipify-perl test00.cu > test00Hipify.cpp
or
../../HIPIFY/dist/hipify-clang (does not work)

